/*
 * Aurora Store
 * Copyright (C) 2018  Rahul Kumar Patel <whyorean@gmail.com>
 *
 * Yalp Store
 * Copyright (C) 2018 Sergey Yeriomin <yeriomin@gmail.com>
 *
 * Aurora Store (a fork of Yalp Store )is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * Aurora Store is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Aurora Store.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.dragons.aurora.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.dragons.aurora.R;
import com.dragons.aurora.fragment.PreferenceFragment;
import com.percolate.caffeine.PhoneUtils;

public abstract class BaseActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    protected boolean isConnected() {
        return PhoneUtils.isNetworkAvailable(this);
    }

    protected int getThemeFromPref() {
        String Theme = PreferenceFragment.getString(this, "PREFERENCE_THEME");
        switch (Theme) {
            case "Light":
                return R.style.AppTheme;
            case "Dark":
                return R.style.AppTheme_Dark;
            case "Black":
                return R.style.AppTheme_Black;
            default:
                return R.style.AppTheme;
        }
    }
}
